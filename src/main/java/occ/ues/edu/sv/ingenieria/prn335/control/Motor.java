/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package occ.ues.edu.sv.ingenieria.prn335.control;

import static java.lang.System.out;
import java.util.ArrayList;

import occ.ues.edu.sv.ingenieria.prn335.entity.Sucursal;

/**
 *
 * @author Esperanza
 */
public class Motor {

    public static ArrayList<Sucursal> sucursales = new ArrayList<Sucursal>();

    public ArrayList<Sucursal> buscarActivos(String departamento, String contacto​) {
//sucursales.get(0);
        ArrayList<Sucursal> sucursales2 = new ArrayList<Sucursal>();

        int size, pos;
        sucursales2.clear();
        size = sucursales.size();

        for (int i = 0; i < size; i++) {
            String depa = sucursales.get(i).getDepartamento();
            String conta = sucursales.get(i).getContacto();
            if (depa.equals(departamento)) {
                if (conta.equals(contacto)) {
                    out.println("\"<h1> nels </h1>\"");
                } else {
                    sucursales2.add(sucursales.get(i));

                }

            }

        }
        return sucursales2;
    }

    public Boolean moificarSucursal(int id_sucursal, String nombre, String ciudad, String departamento, String contacto, boolean estado) {
       boolean Status=false;
        for (int i = 0; i < sucursales.size(); i++) {
            String id = String.valueOf(sucursales.get(i).getId_sucursal());
            String Nombre = sucursales.get(i).getNombre();
            String Ciudad = sucursales.get(i).getCiudad();
            String Departamento = sucursales.get(i).getDepartamento();

            String Contacto = sucursales.get(i).getContacto();

           //sucursales.set(i, new Sucursal(id_sucursal, nombre, ciudad, departamento,contacto, estado));
           if(id.equals(String.valueOf(id_sucursal))){
                           boolean Estado =sucursales.get(i).isEstado();
               if(Estado==false){
               sucursales.set(i, new Sucursal(id_sucursal, nombre, ciudad, departamento,contacto, estado));
               Status=true;
               }else{
                   Status=false;
               }
           }
 
        }    
        return Status;
    }

    public Motor() {

        sucursales.add(new Sucursal(1, "Cinepolis Metrocentro", "Santa Ana", "Santa Ana", "Juan Perez", true));
        sucursales.add(new Sucursal(2, "Cinepolis La Gran Via", "Antiguo Cuscatlan", "San Salvador", "Luis Lopez", true));
        sucursales.add(new Sucursal(3, "Cinepolis Metrocento SM", "San Miguel", "San Miguel", "Will Salgado", false));
        sucursales.add(new Sucursal(4, "Cinemark Metrocentro", "Soyapango", "San Salvador", "Encargado 1", false));
        sucursales.add(new Sucursal(5, "Cinepolis Metrocentro Sonso", "Sonsonate", "Sonsonate", "Encargado 2", true));
        sucursales.add(new Sucursal(6, "Cine La Union", "Puerto de la union", "La Union", "Encargado 3", true));
        sucursales.add(new Sucursal(7, "Cine La Union 2", "Ciudad de la union", "La Union", "Encargado 3", false));
    }

    public ArrayList<Sucursal> mostrarSucursales() {

        return sucursales;
    }

}
