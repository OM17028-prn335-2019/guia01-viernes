/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package occ.ues.edu.sv.ingenieria.prn335.boundary;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import occ.ues.edu.sv.ingenieria.prn335.control.Motor;
import occ.ues.edu.sv.ingenieria.prn335.entity.Sucursal;

/**
 *
 * @author jose
 */
@WebServlet(name = "FrmServlet", urlPatterns = {"/FrmServlet"})
public class FrmServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            Motor m = new Motor();

            ArrayList<Sucursal> s = new ArrayList<Sucursal>();

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FrmServlet</title>");
            out.println("</head>");
            out.println("<body>");
            String btneditar = request.getParameter("editar");
            String btnfiltrar = request.getParameter("filtrar");

            if (btnfiltrar != null) {
                s = m.buscarActivos(request.getParameter("departamento"), request.getParameter("contacto"));

                out.println("<h1> LISTA FILTRADA </h1>");

                out.print("  <table border=\"1\">\n"
                        + "            <thead>\n"
                        + "                <tr>\n"
                        + "                    <th>ID</th>\n"
                        + "                    <th>Nombre</th>\n"
                        + "                    <th>Ciudad</th>\n"
                        + "                    <th>Departamento</th>\n"
                        + "                    <th>Contacto</th>\n"
                        + "                    <th>Estado</th>\n"
                   
                        + "                </tr>\n"
                        + "            </thead>\n"
                        + "            <tbody>");
                Sucursal Sucursal = new Sucursal();
                //request.getParameter("id_sucursal")
                int size = 0;
                size = s.size();

                for (int i = 0; i < s.size(); i++) {
                    out.print("<tr>");
                    out.print("<td>");
                    out.print(s.get(i).getId_sucursal());
                    out.print("</td>");
                    out.print("<td>");
                    out.print(s.get(i).getNombre());
                    out.print("</td>");
                    out.print("<td>");
                    out.print(s.get(i).getCiudad());
                    out.print("</td>");
                    out.print("<td>");
                    out.print(s.get(i).getDepartamento());
                    out.print("</td>");
                    out.print("<td>");
                    out.print(s.get(i).getContacto());
                    out.print("</td>");
                    out.print("<td>");
                    out.print(s.get(i).isEstado());
                    out.print("</td>");
                   

                    out.print("</tr>");

                }

                out.print(" </tbody>\n"
                        + "        </table>\n"
                        + "<br/>\n"
                        + "     \n"
                        + "        <a href=\"index.html\">Volver Al Formulario</a>");

                out.println("</body>");
                s.clear();
                out.println("</html>");
            }
            if (btneditar != null) {
                int id=Integer.parseInt(request.getParameter("id_sucursal"));
               String nombre=request.getParameter("nombre");
               String ciudad=request.getParameter("ciudad");
               String departamento=request.getParameter("departamento");
               String contacto=request.getParameter("contacto");
               boolean estado=Boolean.parseBoolean(request.getParameter("estado"));
               boolean status = m.moificarSucursal(id, nombre, ciudad, departamento, contacto, estado);
                if (status) {
                    out.println("<h1> LISTA MODIFICADA!  </h1>");

                    out.print("  <table border=\"1\">\n"
                            + "            <thead>\n"
                            + "                <tr>\n"
                            + "                    <th>ID</th>\n"
                            + "                    <th>Nombre</th>\n"
                            + "                    <th>Ciudad</th>\n"
                            + "                    <th>Departamento</th>\n"
                            + "                    <th>Contacto</th>\n"
                            + "                    <th>Estado</th>\n"
              
                            + "                </tr>\n"
                            + "            </thead>\n"
                            + "            <tbody>");
                    for (Sucursal sucursal : Motor.sucursales) {
                        out.print("<tr>");
                        out.print("<td>");
                        out.print(sucursal.getId_sucursal());
                        out.print("</td>");
                        out.print("<td>");
                        out.print(sucursal.getNombre());
                        out.print("</td>");
                        out.print("<td>");
                        out.print(sucursal.getCiudad());
                        out.print("</td>");
                        out.print("<td>");
                        out.print(sucursal.getDepartamento());
                        out.print("</td>");
                        out.print("<td>");
                        out.print(sucursal.getContacto());
                        out.print("</td>");
                        out.print("<td>");
                        out.print(sucursal.isEstado());
                        out.print("</td>");
                        out.print("</tr>");
                    }Motor.sucursales.clear();
                    out.print(" </tbody>\n"
                            + "        </table>\n"
                            + "<br/>\n"
                            + "     \n"
                            + "        <a href=\"index.html\">Volver Al Formulario</a>");

                    out.println("</body>");
                    s.clear();
                    out.println("</html>");
                }else{
                out.print("<a href=\"index.html\">Ocurrio un error el la sucursal se encuentra activa. Volver al formulario</a>");
                }
            }

        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
